/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import ContactApp from './src/screens/MiniProject1/index';
import ContactState from './src/screens/MiniProject1/context/ContactState';
import AppNavigation from './src/navigation/routes';
import firebase from '@react-native-firebase/app';

const firebaseConfig = {
  apiKey: 'AIzaSyCGTnYgO0bvI2evmQSU3CEBgh4WcOAt99g',
  authDomain: 'sanberapp-1f06f.firebaseapp.com',
  databaseURL: 'https://sanberapp-1f06f.firebaseio.com',
  projectId: 'sanberapp-1f06f',
  storageBucket: 'sanberapp-1f06f.appspot.com',
  messagingSenderId: '1091378500174',
  appId: '1:1091378500174:web:a0b8faefe1588a3c82e91f',
  measurementId: 'G-ST2GM25V34',
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  return <AppNavigation />;
};

export default App;
