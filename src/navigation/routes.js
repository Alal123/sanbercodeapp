/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Intro from '../screens/Tugas6/Intro';
import SplashScreens from '../screens/Tugas6/SplashScreen';
import Login from '../screens/Tugas7/Login';
import Register from '../screens/Register';
import OtpRegister from '../screens/OtpRegister';
import SetupPassword from '../screens/SetupPassword';
import Tugas3 from '../screens/Tugas2/index';
import ProfileEdit from '../screens/ProfileEdit';
import Help from '../screens/Help';
import MainTab from './MainTab';
import Statistik from '../screens/Statistik';
import AuthLoad from '../screens/AuthLoad';
import Riwayat from '../screens/Riwayat';
import Donasi from '../screens/Donasi';

const Stack = createStackNavigator();

const MainNavigation = () => (
  <Stack.Navigator initialRouteName="AuthLoad">
    <Stack.Screen
      name="AuthLoad"
      component={AuthLoad}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Register"
      component={Register}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="OtpRegister"
      component={OtpRegister}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="SetupPassword"
      component={SetupPassword}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Profile"
      component={Tugas3}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="ProfileEdit"
      component={ProfileEdit}
      options={{headerShown: false}}
    />
    <Stack.Screen name="Help" component={Help} options={{headerShown: false}} />
    <Stack.Screen
      name="MainTab"
      component={MainTab}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Statistik"
      component={Statistik}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Riwayat"
      component={Riwayat}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Donasi"
      component={Donasi}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

const AppNavigation = () => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
  }, []);

  if (isLoading) {
    return <SplashScreens />;
  }

  return (
    <NavigationContainer>
      <MainNavigation />
    </NavigationContainer>
  );
};

export default AppNavigation;
