/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import api from '../../api';
import axios from 'axios';
import {GoogleSignin} from '@react-native-community/google-signin';
import {CommonActions} from '@react-navigation/native';
const Tugas2 = (props) => {
  const {navigation} = props;
  const [profileData, setProfile] = useState({});
  useEffect(() => {
    getProfile();
  }, []);

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      return token;
    } catch (err) {
      console.log(err);
    }
  };

  const getProfile = async () => {
    try {
      const token = await getToken();
      if (token == null) {
        getCurrentUser();
      }
      const response = await axios.get(`${api}/profile/get-profile`, {
        timeout: 2000,
        headers: {
          Authorization: 'Bearer' + token,
        },
      });
      setProfile(response.data.data.profile);
    } catch (error) {
      console.log('error_getprofile', error);
    }
  };

  const getCurrentUser = async () => {
    const userInfo = await GoogleSignin.signInSilently();
    console.log('data from google', userInfo);
    setProfile(userInfo);
  };

  const onLogout = async () => {
    try {
      const isSigned = await GoogleSignin.isSignedIn();

      if (isSigned) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
      await AsyncStorage.removeItem('token');
      setProfile({});
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: 'Intro'}],
        }),
      );
    } catch (error) {
      console.log('error-logout', error);
    }
  };
  const header = () => {
    return (
      <View style={Styles.headerContainer}>
        <Text style={Styles.headerText}>Account</Text>
      </View>
    );
  };

  const renderTop = () => {
    const photopath = `https://crowdfunding.sanberdev.com/${profileData.photo}`;
    const fullName =
      (profileData && profileData.user && profileData.user.name) ||
      profileData.name;
    const photoUri =
      (profileData && profileData.user && profileData.user.photo) || photopath;
    return (
      <View
        style={{
          backgroundColor: '#ffff',
        }}>
        <TouchableOpacity
          style={Styles.profileContainer}
          onPress={() =>
            navigation.navigate('ProfileEdit', {
              name: fullName,
              photo: photoUri,
              email: profileData.email,
            })
          }>
          <Image
            source={{
              uri: photoUri,
            }}
            style={Styles.profilePicture}
          />
          <Text style={Styles.profileName}>{fullName}</Text>
        </TouchableOpacity>
        <View style={Styles.horizontalLine} />
        <View style={Styles.saldoContainer}>
          <View style={Styles.iconContainer}>
            <Icons name="wallet-outline" size={25} style={Styles.iconStyle} />
            <Text>Saldo</Text>
          </View>
          <Text>Rp 1.200.000</Text>
        </View>
      </View>
    );
  };

  const renderContent = () => {
    return (
      <View>
        <View style={[Styles.horizontalLine, {height: 5}]} />
        <View style={Styles.contentContainer}>
          <Icons name="settings-outline" size={25} style={Styles.iconStyle} />
          <Text>Pengaturan</Text>
        </View>
        <View style={Styles.horizontalLine} />
        <TouchableOpacity style={Styles.contentContainer} onPress={() => navigation.navigate('Help')}>
          <Icons
            name="help-circle-outline"
            size={25}
            style={Styles.iconStyle}
          />
          <Text>Bantuan</Text>
        </TouchableOpacity>
        <View style={Styles.horizontalLine} />
        <View style={Styles.contentContainer}>
          <Icons name="book-outline" size={25} style={Styles.iconStyle} />
          <Text>Syarat & Ketentuan</Text>
        </View>
        <View style={[Styles.horizontalLine, {height: 6}]} />
      </View>
    );
  };
  const renderBottom = () => {
    return (
      <TouchableOpacity style={Styles.contentContainer} onPress={onLogout}>
        <Icons name="exit-outline" size={25} style={Styles.iconStyle} />
        <Text>Keluar</Text>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView>
      {header()}
      {renderTop()}
      {renderContent()}
      {renderBottom()}
    </SafeAreaView>
  );
};

const Styles = StyleSheet.create({
  headerContainer: {
    height: 65,
    backgroundColor: 'rgba(52,137,247,1)',
    justifyContent: 'center',
    paddingLeft: 16,
  },
  headerText: {
    fontWeight: 'bold',
    color: '#ffff',
    fontSize: 20,
  },
  profileContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
  },
  horizontalLine: {
    height: 1,
    backgroundColor: 'rgba(245,245,245,1)',
  },
  profilePicture: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  profileName: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  saldoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    marginVertical: 16,
  },
  iconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentContainer: {
    flexDirection: 'row',
    backgroundColor: '#ffff',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  iconStyle: {
    marginRight: 16,
  },
});

export default Tugas2;
