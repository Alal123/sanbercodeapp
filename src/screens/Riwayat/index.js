import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, StyleSheet} from 'react-native';
import {Header} from '../../components';
import {colors} from '../../configs';
import api from '../../api';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Riwayat = (props) => {
  const [history, setHistory] = useState([]);
  const {navigation} = props;
  useEffect(() => {
    getHistory();
  }, []);

  const getHistory = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      const response = await axios.get(`${api}/donasi/riwayat-transaksi`, {
        timeout: 2000,
        headers: {
          Authorization: 'Bearer' + token,
        },
      });
      setHistory(response.data.data.riwayat_transaksi);
    } catch (error) {
      console.log('error_getHistory', error);
    }
  };

  const renderItem = (item) => {
    return (
      <View style={styles.historyContainer}>
        <Text style={styles.titleText}>Total {item.amount}</Text>
        <Text>Donasi ID : {item.order_id}</Text>
        <Text>Tanggal Transaksi : {item.created_at}</Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header title="Riwayat" onPress={() => navigation.pop()} />
      <FlatList
        keyExtractor={(item) => item.id.toString()}
        data={history}
        renderItem={({item}) => renderItem(item)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  titleText: {
    fontWeight: 'bold',
  },
  historyContainer: {
    backgroundColor: colors.white,
    marginBottom: 5,
    padding: 16,
    justifyContent: 'center',
  },
});

export default Riwayat;
