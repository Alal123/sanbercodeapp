import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {colors} from '../../configs';
import Icon from 'react-native-vector-icons/Ionicons';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoiYWxhbG11aGFtbWFkIiwiYSI6ImNraG1vYWw2cDFrYWcycHB0dzN1a2Rqd3gifQ.fdXaSRvHbiEXGeGSDsd1Zw',
);

const Help = () => {
  const coordinates = [
    [107.58011, -6.890066],
    [106.819449, -6.218465],
    [110.365231, -7.795766],
  ];
  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = await MapboxGL.requestAndroidLocationPermissions();
      } catch (error) {
        console.log('error_getPermision', error);
      }
    };
    getLocation();
  }, []);
  return (
    <View style={styles.container}>
      <MapboxGL.MapView
        style={{
          flex: 1,
        }}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {coordinates.map((data, idx) => {
          return (
            <MapboxGL.PointAnnotation id={idx.toString()} coordinate={data}>
              <MapboxGL.Callout
                title={`Longitude ${data[0]}\nLatitide ${data[1]}`}
              />
            </MapboxGL.PointAnnotation>
          );
        })}
      </MapboxGL.MapView>
      <View style={styles.bottombox}>
        <View style={styles.rowContainer}>
          <Icon name="home" size={20} style={{marginRight:10}} />
          <Text>Jakarta, Bandung, Yogyakarta</Text>
        </View>
        <View style={styles.rowContainer}>
          <Icon name="at-circle" size={20} style={{marginRight:10}} />
          <Text>customer_service@crowdfunding.com</Text>
        </View>
        <View style={styles.rowContainer}>
          <Icon name="call" size={20} style={{marginRight:10}} />
          <Text>(021) 7777-888</Text>
        </View>

      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bottombox: {
    backgroundColor: colors.white,
    height: 270,
    width: '100%',
    paddingHorizontal: 16,
    paddingVertical: 16,
    position: 'absolute',
    justifyContent: 'space-between',
    bottom: 0,
    paddingBottom: 50,
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
    borderBottomWidth: 1,
    paddingBottom: 16,
    borderBottomColor: colors.grey,
  },
});

export default Help;
