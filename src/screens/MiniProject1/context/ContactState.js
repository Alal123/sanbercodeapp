import React, {useReducer} from 'react';
import ContactContext from './ContactContext';
import ContactReducer from './ContactReducer';
import {ADD_CONTACT, DEL_CONTACT} from './types';

const ContactState = (props) => {
  const initialState = {
    contacts: [],
  };

  const [state, dispatch] = useReducer(ContactReducer, initialState);

  const onHandleAddContact = (data) => {
    dispatch({
      type: ADD_CONTACT,
      payload: data,
    });
  };

  const onDeleteContact = (id) => {
    dispatch({
      type: DEL_CONTACT,
      payload: id,
    });
  };
  return (
    <ContactContext.Provider
      value={{
        contacts: state.contacts,
        onHandleAddContact,
        onDeleteContact,
      }}>
      {props.children}
    </ContactContext.Provider>
  );
};

export default ContactState;
