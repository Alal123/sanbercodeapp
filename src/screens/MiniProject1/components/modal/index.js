import React, {useState, useContext} from 'react';
import {
  View,
  Text,
  Modal,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../config/Colors';
import ContactContext from '../../context/ContactContext';

const ModalContact = (props) => {
  const contactContext = useContext(ContactContext);
  const {onClose, isActive} = props;
  const {onHandleAddContact, contacts} = contactContext;
  const [formdata, setFormData] = useState({
    id: '',
    firstName: '',
    lastName: '',
    noHp: '',
  });

  const onSubmit = () => {
    if (
      formdata.firstName === '' ||
      formdata.lastName === '' ||
      formdata.noHp === ''
    ) {
      return;
    }

    onHandleAddContact({
      ...formdata,
      id: contacts.length + 1,
    });
    setFormData({
      ...formdata,
      id: '',
      firstName: '',
      lastName: '',
      noHp: '',
    });
    onClose();
  };

  const handleClose = () => {
    onClose();
    setFormData({
      ...formdata,
      firstName: '',
      lastName: '',
      noHp: '',
    });
  };
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isActive}
      onRequestClose={onClose}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <View style={styles.headerModal}>
            <Text style={styles.modalTitle}>Tambah Kontak</Text>
            <TouchableOpacity style={styles.closeButton} onPress={handleClose}>
              <Icon name="times-circle" size={20} color={Colors.main.red} />
            </TouchableOpacity>
          </View>
          <View style={styles.modalBody}>
            <TextInput
              value={formdata.firstName}
              placeholder="First Name"
              style={styles.input}
              onChangeText={(firstName) =>
                setFormData({
                  ...formdata,
                  firstName,
                })
              }
            />
            <TextInput
              value={formdata.lastName}
              placeholder="Last Name"
              style={styles.input}
              onChangeText={(lastName) =>
                setFormData({
                  ...formdata,
                  lastName,
                })
              }
            />
            <TextInput
              value={formdata.noHp}
              placeholder="Nomor Handphone"
              style={styles.input}
              keyboardType="numeric"
              maxLength={12}
              onChangeText={(noHp) =>
                setFormData({
                  ...formdata,
                  noHp,
                })
              }
            />
          </View>
          <View style={styles.bottom}>
            <TouchableOpacity onPress={onSubmit}>
              <Text>Simpan</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleClose}>
              <Text>Batal</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.main.transparentblack,
  },
  headerModal: {
    height: 50,
    backgroundColor: Colors.main.primary,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 16,
  },
  input: {
    borderWidth: 1,
    borderRadius: 16,
    marginBottom: 16,
    borderColor: Colors.main.grey,
    height: 50,
    paddingHorizontal: 16,
  },
  closeButton: {
    height: 20,
    width: 20,
    position: 'absolute',
    right: 5,
  },
  modalView: {
    marginHorizontal: 16,
    backgroundColor: 'white',
    borderRadius: 16,
    elevation: 5,
  },
  modalBody: {
    paddingHorizontal: 16,
    justifyContent: 'space-between',
  },
  modalTitle: {
    textAlign: 'center',
    color: Colors.main.white,
    fontWeight: 'bold',
    fontSize: 16,
  },
  bottom: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 16,
  },
});

export default ModalContact;
