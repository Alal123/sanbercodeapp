import React, {useState, useContext} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import Header from './components/header';
import ModalContact from './components/modal';
import ListContact from './components/listcontact';
import Colors from './config/Colors';
import ContactContext from './context/ContactContext';

const Contacts = () => {
  const [isShowModal, setModal] = useState(false);
  const contactContext = useContext(ContactContext);
  const {contacts, onDeleteContact} = contactContext;
  const renderItem = (item) => {
    return (
      <ListContact
        item={item}
        onPressDel={() => {
          onDeleteContact(item.id);
        }}
      />
    );
  };
  return (
    <View style={styles.container}>
      <Header
        color={Colors.main.primary}
        title="Contact App"
        right
        onPress={() => setModal(true)}
      />
      <View
        style={{
          marginTop: 16,
          paddingBottom: 32,
        }}>
        <FlatList
          keyExtractor={(item) => item.id.toString()}
          data={contacts}
          renderItem={({item}) => renderItem(item)}
        />
      </View>
      <ModalContact isActive={isShowModal} onClose={() => setModal(false)} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.main.white,
  },
});

export default Contacts;
