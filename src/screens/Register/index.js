/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import {Header, Button} from '../../components';
import {colors} from '../../configs';
import Icons from 'react-native-vector-icons/Ionicons';
import api from '../../api';
import axios from 'axios';

const Register = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [fullName, setFullName] = useState('');

  const onRegister = async () => {
    if (email === '' || fullName === '') {
      return;
    }
    try {
      const data = {
        name: fullName,
        email,
      };
      const response = await axios.post(`${api}/auth/register`, data, {
        timeout: 2000,
      });
      navigation.navigate('OtpRegister', {
        email: response.data.data.user.email,
      });
    } catch (error) {
      const errorEmail = error.response.data.errors.email[0].replace(
        'The email has already been taken',
        'email telah terdaftar',
      );
      alert(errorEmail);
      console.log('error', error);
    }
  };
  const renderTop = () => {
    return (
      <View style={styles.topContainer}>
        <Text style={styles.templateText}>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores
          dolores odio non neque illum delectus, deleniti velit adipisci magni
          facere doloremque excepturi commodi voluptate numquam. Earum in
          obcaecati delectus recusandae.
        </Text>
      </View>
    );
  };

  const renderForm = () => {
    return (
      <View>
        <View>
          <Text>Nomor Ponsel atau Email</Text>
          <TextInput
            underlineColorAndroid={colors.grey}
            value={email}
            onChangeText={(text) => setEmail(text)}
          />
        </View>
        <View>
          <Text>Nama Lengkap</Text>
          <TextInput
            underlineColorAndroid={colors.grey}
            value={fullName}
            onChangeText={(text) => setFullName(text)}
          />
        </View>
        <Button style={styles.btnRegister} onPress={onRegister}>
          <Text style={styles.registerText}>Daftar</Text>
        </Button>
        <View style={styles.mt10}>
          <Text style={{textAlign: 'center', color: colors.grey}}>
            Sudah punya akun?{' '}
            <Text
              style={{
                color: colors.blue,
              }}>
              Masuk
            </Text>
          </Text>
        </View>
      </View>
    );
  };

  const renderBottom = () => {
    return (
      <View style={styles.bottomContainer}>
        <Text style={styles.textTitleBottom}>Atau lebih cepat dengan</Text>
        <View style={styles.btnBottomContainer}>
          <Button style={styles.btnBottom}>
            <Icons name="logo-facebook" size={20} color={colors.blue} />
            <Text>Facebook</Text>
          </Button>
          <Button style={styles.btnBottom}>
            <Icons name="logo-google" size={20} />
            <Text>Facebook</Text>
          </Button>
        </View>
        <View
          style={{
            paddingHorizontal: 16,
          }}>
          <Text style={{color: colors.grey}}>
            Denga mendaftar, kamu setuju dengan{' '}
            <Text style={{textDecorationLine: 'underline'}}>
              Syarat dan Ketentuan
            </Text>
            CrowFunding.com
          </Text>
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header title="Daftar" onPress={() => navigation.pop()} />
      <View style={styles.padder}>
        <View>
          {renderTop()}
          {renderForm()}
        </View>
        {renderBottom()}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  padder: {
    paddingHorizontal: 16,
    flex: 1,
    justifyContent: 'space-between',
  },
  topContainer: {
    paddingVertical: 16,
  },
  templateText: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  btnRegister: {
    backgroundColor: colors.blue,
    height: 40,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  registerText: {
    color: colors.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
  btnBottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  bottomContainer: {
    paddingBottom: 16,
    height: 150,
    justifyContent: 'space-between',
  },
  textTitleBottom: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 10,
  },
  btnBottom: {
    flexDirection: 'row',
    height: 35,
    borderWidth: 1,
    borderColor: colors.lightGrey,
    alignItems: 'center',
    paddingHorizontal: 16,
    flex: 0.4,
    borderRadius: 4,
  },
  mt10: {
    marginTop: 10,
  },
});

export default Register;
