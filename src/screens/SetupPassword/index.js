import React, {useState} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import {Header, Button} from '../../components';
import {colors} from '../../configs';
import Icons from 'react-native-vector-icons/Ionicons';
import api from '../../api';
import axios from 'axios';
import {CommonActions} from '@react-navigation/native';

const SetupPassword = (props) => {
  const [password, setPassword] = useState('');
  const [isShowPassword, setShowPassword] = useState(true);
  const [isShowConfirmPassword, setShowConfirmPassword] = useState(true);
  const [confirmPassword, setConfirmPassword] = useState('');
  const {navigation, route} = props;
  const {
    params: {email},
  } = route;

  const onPressSave = async () => {
    if (password === '' || confirmPassword === '') {
      return;
    }
    if (password !== confirmPassword) {
      alert('password dan konfirmasi password tidak sama');
      return;
    }
    try {
      const data = {
        email: email,
        password,
        password_confirmation: confirmPassword,
      };
      const response = await axios.post(`${api}/auth/update-password`, data, {
        timeout: 2000,
      });
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: 'Intro'}],
        }),
      );
    } catch (error) {
      console.log('error_setup_password', error);
    }
  };

  const renderForm = () => {
    return (
      <View>
        <View style={styles.inputContainer}>
          <TextInput
            value={password}
            secureTextEntry={isShowPassword}
            onChangeText={(text) => setPassword(text)}
            underlineColorAndroid={colors.grey}
            style={styles.input}
            placeholder="Password"
          />
          <Button onPress={() => setShowPassword(!isShowPassword)}>
            <Icons
              name={!isShowPassword ? 'eye-outline' : 'eye-off-outline'}
              size={25}
              color={colors.grey}
            />
          </Button>
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            value={confirmPassword}
            secureTextEntry={isShowConfirmPassword}
            onChangeText={(text) => setConfirmPassword(text)}
            underlineColorAndroid={colors.grey}
            style={styles.input}
            placeholder="Confirm Password"
          />
          <Button
            onPress={() => setShowConfirmPassword(!isShowConfirmPassword)}>
            <Icons
              name={!isShowConfirmPassword ? 'eye-outline' : 'eye-off-outline'}
              size={25}
              color={colors.grey}
            />
          </Button>
        </View>

        <Button style={styles.btnSave} onPress={onPressSave}>
          <Text style={styles.saveText}>Simpan</Text>
        </Button>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header title="Ubah Kata Sandi" onPress={() => navigation.pop()} />
      <View style={styles.contentContainer}>
        <Text style={styles.title}>
          Tentukan kata sandi baru untuk keamanan akun kamu
        </Text>
        {renderForm()}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  contentContainer: {
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 16,
  },
  input: {
    flex: 1,
  },
  btnSave: {
    backgroundColor: colors.blue,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
  },
  saveText: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: 18,
  },
});

export default SetupPassword;
