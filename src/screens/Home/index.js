/* eslint-disable react-native/no-inline-styles */
import React, {useRef} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  FlatList,
} from 'react-native';
import {colors} from '../../configs';
import Icons from 'react-native-vector-icons/Ionicons';
import AppIntroSlider from 'react-native-app-intro-slider';

let img1, img2, img3, img4, img5;
img1 =
  'https://images.unsplash.com/photo-1526304640581-d334cdbbf45e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80';
img2 =
  'https://images.unsplash.com/photo-1579621970563-ebec7560ff3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=751&q=80';
img3 =
  'https://images.unsplash.com/photo-1520695082671-948f2937d752?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80';
img4 =
  'https://images.unsplash.com/photo-1558690048-11fb8202ce38?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80';
img5 =
  'https://images.unsplash.com/photo-1544027993-37dbfe43562a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80';
const sliderImages = [img1, img2, img3, img4, img5];

const Home = (props) => {
  const {navigation} = props;
  const slider = useRef();

  const renderHeader = () => {
    return (
      <View style={styles.header}>
        <Icons name="heart-circle-outline" size={25} color={colors.red} />
        <View style={styles.inputContainer}>
          <Text style={styles.inputPlaceHolder}>Cari disini</Text>
        </View>
        <Icons name="heart" size={25} color={colors.red} />
      </View>
    );
  };

  const renderTopMenu = () => {
    return (
      <View style={styles.absoluteMenu}>
        <View style={styles.topMenuContainer}>
          <View style={styles.walletContainer}>
            <View>
              <Icons name="wallet" size={20} color={colors.white} />
              <Text style={styles.walletText}>Rp. 0</Text>
            </View>
            <Text style={styles.walletText}>Saldo</Text>
          </View>
          <TouchableOpacity style={styles.topIconMenu}>
            <Icons name="add" size={15} />
            <Text>Isi</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.topIconMenu}>
            <Icons name="infinite" size={15} />
            <Text>Riwayat</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.topIconMenu}>
            <Icons name="grid-outline" size={15} />
            <Text>Lainnya</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const renderItem = (img) => {
    return (
      <View style={styles.sliderContainer}>
        <Image
          source={{uri: img}}
          style={styles.sliderPicture}
          resizeMode="contain"
        />
      </View>
    );
  };

  const renderPagination = (activeIdx) => {
    return (
      <View style={styles.paginationContainer}>
        <SafeAreaView>
          <View style={styles.paginationDots}>
            {sliderImages.length > 1 &&
              sliderImages.map((_, i) => (
                <TouchableOpacity
                  key={i}
                  style={[
                    styles.dot,
                    i === activeIdx
                      ? {backgroundColor: colors.blue}
                      : {backgroundColor: colors.white},
                  ]}
                  onPress={() => slider.current.goToSlide(i, true)}
                />
              ))}
          </View>
        </SafeAreaView>
      </View>
    );
  };

  const renderSlider = () => {
    return (
      <View
        style={{
          height: 300,
        }}>
        <AppIntroSlider
          ref={(ref) => (slider.current = ref)}
          data={sliderImages}
          keyExtractor={(idx) => idx}
          renderItem={({item}) => renderItem(item)}
          dotStyle={{backgroundColor: 'rgba(0, 0, 0, .2)'}}
          activeDotStyle={{backgroundColor: colors.blue}}
          renderPagination={(idx) => renderPagination(idx)}
        />
      </View>
    );
  };

  const renderBottomMenu = () => {
    return (
      <>
        <View
          style={{
            flexDirection: 'row',
            marginBottom: 16,
          }}>
          <TouchableOpacity style={styles.bottomIconMenu}>
            <Icons name="cash" size={20} color={colors.blue} />
            <Text>Donasi</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.bottomIconMenu}
            onPress={() => navigation.navigate('Statistik')}>
            <Icons name="stats-chart" size={20} color={colors.blue} />
            <Text>Statistik</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.bottomIconMenu}
            onPress={() => navigation.navigate('Riwayat')}>
            <Icons name="newspaper" size={20} color={colors.blue} />
            <Text>Riwayat</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.bottomIconMenu}
            onPress={() => navigation.navigate('Donasi')}>
            <Icons name="help-circle" size={20} color={colors.blue} />
            <Text>Bantu</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.lineBorder} />
      </>
    );
  };

  const renderNews = () => {
    return (
      <FlatList
        horizontal
        contentContainerStyle={{
          marginTop: 16,
        }}
        showsHorizontalScrollIndicator={false}
        data={sliderImages}
        keyExtractor={(idx) => idx}
        renderItem={({item}) => (
          <View style={styles.newContainer}>
            <Image
              source={{uri: item}}
              style={{height: 100, width: 250}}
              resizeMode="cover"
            />
            <View
              style={{
                marginTop: 6,
                marginLeft: 6,
              }}>
              <Text>Lorem ipsum dolor sit amet.</Text>
            </View>
          </View>
        )}
      />
    );
  };

  return (
    <ScrollView style={styles.container}>
      {renderHeader()}
      {renderTopMenu()}
      {renderSlider()}
      {renderBottomMenu()}
      {renderNews()}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  header: {
    backgroundColor: colors.blue,
    height: 100,
    paddingVertical: 16,
    paddingHorizontal: 16,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  inputContainer: {
    flex: 0.9,
    borderWidth: 1,
    borderColor: colors.white,
    height: 30,
    borderRadius: 16,
    justifyContent: 'center',
    paddingLeft: 16,
  },
  inputPlaceHolder: {
    color: colors.white,
  },
  topMenuContainer: {
    backgroundColor: colors.white,
    elevation: 2,
    height: 70,
    marginHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    borderRadius: 8,
    top: -40,
    left: 0,
    right: 0,
    zIndex: 99,
    position: 'absolute',
  },
  absoluteMenu: {
    height: 40,
  },
  walletContainer: {
    backgroundColor: colors.blue,
    flexDirection: 'row',
    flex: 1.2,
    padding: 5,
    borderRadius: 8,
    height: 50,
  },
  walletText: {
    color: colors.white,
  },
  topIconMenu: {
    flex: 1,
    marginHorizontal: 10,
    alignItems: 'center',
    height: 50,
    justifyContent: 'center',
  },
  bottomIconMenu: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    height: 30,
  },
  paginationContainer: {
    position: 'absolute',
    top: 220,
    left: 16,
    right: 16,
  },
  paginationDots: {
    height: 20,
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 8,
  },
  sliderContainer: {
    height: 300,
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
  sliderPicture: {
    height: 300,
    borderRadius: 8,
  },
  lineBorder: {
    height: 10,
    backgroundColor: colors.lightGrey,
  },
  newContainer: {
    borderWidth: 1,
    overflow: 'hidden',
    marginHorizontal: 10,
    flex: 1,
    borderColor: colors.lightGrey,
    borderRadius: 16,
    elevation: 2,
    height: 150,
  },
});

export default Home;
