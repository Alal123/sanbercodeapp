/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef} from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  Modal,
  Image,
  StyleSheet,
} from 'react-native';
import {Header, Button} from '../../components';
import {TextInputMask} from 'react-native-masked-text';
import Icons from 'react-native-vector-icons/Ionicons';
import {colors} from '../../configs';
import {RNCamera} from 'react-native-camera';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import api from '../../api';

const Donasi = (props) => {
  let camera = useRef(null);
  let moneyField = useRef(null);
  const {navigation} = props;
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [form, setForm] = useState({
    photo: null,
    title: '',
    description: '',
    amount: '',
  });
  const {photo, title, description, amount} = form;

  const onChangeText = (value, type) => {
    setForm({
      ...form,
      [type]: value,
    });
  };

  const onAddDonate = async () => {
    if (title === '' || description === '' || amount === '') {
      return;
    }
    try {
      const numberRawValue = moneyField.current.getRawValue();
      const formData = new FormData();
      formData.append('title', title);
      formData.append('description', description);
      formData.append('donation', numberRawValue);
      formData.append('photo', {
        uri: photo.uri,
        name: 'photo.jpg',
        type: 'image/jpg',
      });
      const token = await AsyncStorage.getItem('token');
      const response = await axios.post(
        `${api}/donasi/tambah-donasi`,
        formData,
        {
          timeout: 2000,
          headers: {
            Authorization: 'Bearer' + token,
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
          },
        },
      );
      console.log('test', response);
      alert('Donasi Baru berhasil ditambahkan');
      navigation.pop();
    } catch (error) {
      alert('donasi tidak berhasil di tambahkan');
      console.log('error_add_donasi', error.response);
    }
  };
  const toggleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.current.takePictureAsync(options);
      setForm({
        ...form,
        photo: data,
      });
      setIsVisible(false);
    }
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera style={{flex: 1}} type={type} ref={camera}>
            <View style={{marginLeft: 16, marginTop: 16}}>
              <TouchableOpacity
                style={styles.cameraReverse}
                onPress={toggleCamera}>
                <Icons name="camera-reverse" size={15} />
              </TouchableOpacity>
            </View>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'flex-end',
                paddingBottom: 16,
                flex: 1,
              }}>
              <TouchableOpacity style={styles.btnTake} onPress={takePicture}>
                <Icons name="camera" size={30} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };
  return (
    <View style={styles.container}>
      <Header title="Buat Donasi" onPress={() => navigation.pop()} />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flexGrow: 1, backgroundColor: colors.white}}>
        <ScrollView style={{flex: 1}}>
          {photo === null ? (
            <TouchableOpacity
              style={styles.photoContainer}
              onPress={() => setIsVisible(true)}>
              <Icons name="camera" size={30} />
              <Text>Pilih Gambar</Text>
            </TouchableOpacity>
          ) : (
            <View
              style={{
                paddingHorizontal: 16,
                paddingVertical: 16,
              }}>
              <Image
                source={{uri: photo.uri}}
                style={{
                  height: 200,
                }}
              />
            </View>
          )}

          <View style={styles.formContainer}>
            <View>
              <Text>Judul</Text>
              <TextInput
                value={title}
                placeholder="Judul"
                underlineColorAndroid={colors.lightGrey}
                onChangeText={(text) => onChangeText(text, 'title')}
              />
            </View>
            <View>
              <Text>Deskripsi</Text>
              <TextInput
                value={description}
                placeholder="Deskripsi"
                underlineColorAndroid={colors.lightGrey}
                onChangeText={(text) => onChangeText(text, 'description')}
                numberOfLines={5}
                multiline
                textAlignVertical="top"
              />
            </View>
            <View
              style={{
                marginBottom: 16,
              }}>
              <Text>Dana yang dibutuhkan</Text>
              <TextInputMask
                type={'money'}
                options={{
                  precision: 0,
                  separator: '.',
                  delimiter: '.',
                  unit: 'Rp.',
                  suffixUnit: '',
                }}
                value={amount}
                onChangeText={(text) => onChangeText(text, 'amount')}
                ref={moneyField}
                style={styles.inputAmount}
              />
            </View>
            <Button style={styles.btnCreate} onPress={onAddDonate}>
              <Text style={styles.btnText}>Buat</Text>
            </Button>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      {renderCamera()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  photoContainer: {
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 16,
  },
  inputAmount: {
    borderBottomWidth: 1,
    borderColor: colors.lightGrey,
  },
  btnText: {
    color: colors.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
  btnCreate: {
    backgroundColor: colors.blue,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
  },
  cameraReverse: {
    backgroundColor: colors.white,
    width: 30,
    height: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnTake: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Donasi;
