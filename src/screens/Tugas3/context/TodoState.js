import React, {useReducer} from 'react';
import TodoContext from './TodoContext';
import TodoReducer from './TodoReducer';
import {ADD_TODO, REMOVE_TODO} from './types';

const TodoState = (props) => {
  const initialState = {
    todos: [],
  };

  const [state, dispatch] = useReducer(TodoReducer, initialState);

  const onHandleAddTodo = (data) => {
    dispatch({
      type: ADD_TODO,
      payload: data,
    });
  };

  const onRemoveTodo = (id) => {
    dispatch({
      type: REMOVE_TODO,
      payload: id,
    });
  };
  return (
    <TodoContext.Provider
      value={{
        todos: state.todos,
        onHandleAddTodo,
        onRemoveTodo,
      }}>
      {props.children}
    </TodoContext.Provider>
  );
};

export default TodoState;
