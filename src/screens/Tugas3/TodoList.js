import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';

const TodoList = (props) => {
  const {todoList, onDelete} = props;
  return (
    <View style={styles.container}>
      <FlatList
        keyExtractor={(item) => item.id.toString()}
        data={todoList}
        renderItem={({item}) => (
          <View key={item.id} style={styles.todoContainer}>
            <View>
              <Text>{item.date}</Text>
              <Text>{item.note}</Text>
            </View>
            <TouchableOpacity onPress={() => onDelete(item.id)}>
              <Icons name="trash-outline" size={25} />
            </TouchableOpacity>
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 16,
    marginTop: 20,
  },
  todoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: 'grey',
    elevation: 2,
    shadowColor: 'grey',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    alignItems: 'center',
    padding: 16,
    marginBottom: 16,
  },
});

export default TodoList;
