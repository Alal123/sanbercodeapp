import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';

const TodoInput = (props) => {
  const {value, onChange, onAddTodo} = props;
  return (
    <View style={styles.container}>
      <Text>Masukan TodoList</Text>
      <View style={styles.inputContainer}>
        <TextInput
          autoCompleteType="off"
          autoCorrect={false}
          placeholder="Input Here"
          style={styles.input}
          value={value}
          onChangeText={(text) => onChange(text)}
        />
        <TouchableOpacity
          style={styles.iconContainer}
          onPress={() => onAddTodo(value)}>
          <Icons name="add-outline" size={25} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingTop: 8,
  },
  inputContainer: {
    height: 30,
    flexDirection: 'row',
    marginTop: 8,
  },
  input: {
    height: 40,
    borderWidth: 1,
    paddingLeft: 8,
    flex: 1,
  },
  iconContainer: {
    backgroundColor: 'rgba(40,200,250,1)',
    marginLeft: 6,
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default TodoInput;
