import React, {useContext, useState} from 'react';
import {SafeAreaView, View, Text, StyleSheet} from 'react-native';
import TodoInput from './TodoInput';
import TodoList from './TodoList';
import TodoContext from './context/TodoContext';
import moment from 'moment';

const TodoApp = () => {
  const [inputTodo, setInputTodo] = useState('');
  const todoContext = useContext(TodoContext);
  const {todos, onRemoveTodo, onHandleAddTodo} = todoContext;
  const onAddData = (value) => {
    if (value === '') {
      return;
    }
    const newData = {
      id: todos.length + 1,
      date: moment(new Date()).format('DD/MM/YYYY'),
      note: value,
    };
    onHandleAddTodo(newData);
    setInputTodo('');
  };

  return (
    <SafeAreaView style={styles.container}>
      <TodoInput
        value={inputTodo}
        onChange={(text) => setInputTodo(text)}
        onAddTodo={(data) => onAddData(data)}
      />
      <TodoList todoList={todos} onDelete={(id) => onRemoveTodo(id)} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(242,242,242,1)',
  },
});

export default TodoApp;
