import React from 'react';
import {View, Text, Image, StatusBar, SafeAreaView} from 'react-native';
import styles from './styles';
import {colors} from '../../configs/index';
import {Button} from '../../components/index';
//import module  react native app intro slider
import AppIntroSlider from 'react-native-app-intro-slider';

// data yang akan kita tampilkan sebagai onboarding aplikasi
const data = [
  {
    id: 1,
    image:
      'https://images.unsplash.com/photo-1543269865-cbf427effbad?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
    description: 'Aplikasi Donasi Jutan Orang Baik',
  },
  {
    id: 2,
    image:
      'https://images.unsplash.com/photo-1557804506-669a67965ba0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80',
    description:
      'Berdonasi lah walau sedikit karena kebaikan tidak melihat angka',
  },
  {
    id: 3,
    image:
      'https://images.unsplash.com/photo-1555421689-d68471e189f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
    description:
      'Bersama CrowFunding, Awali hari dengan berdonasi untuk bekal amal ibadah',
  },
];

const Intro = ({navigation}) => {
 
  //tampilan onboarding yang ditampilkan dalam renderItem
  const renderItem = ({item}) => {
    return (
      <View style={styles.listContainer}>
        <View style={styles.listContent}>
          <Image
            source={{uri: item.image}}
            style={styles.imgList}
            resizeMethod="auto"
            resizeMode="contain"
          />
        </View>
        <Text style={styles.textList}>{item.description}</Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
        <View style={styles.textLogoContainer}>
          <Text style={styles.textLogo}>CrowdFunding</Text>
        </View>
        <View style={styles.slider}>
          {/* contoh menggunakan component react native app intro slider */}
          <AppIntroSlider
            data={data} //masukan data yang akan ditampilkan menjadi onBoarding, dia bernilai array
            renderItem={renderItem} // untuk menampilkan onBoarding dar data array
            renderNextButton={() => null}
            renderDoneButton={() => null}
            activeDotStyle={styles.activeDotStyle}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
        <View style={styles.btnContainer}>
          <Button
            style={styles.btnLogin}
            onPress={() => navigation.navigate('Login')}>
            <Text style={styles.btnTextLogin}>MASUK</Text>
          </Button>
          <Button
            style={styles.btnRegister}
            onPress={() => navigation.navigate('Register')}>
            <Text style={styles.btnTextRegister}>DAFTAR</Text>
          </Button>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Intro;
