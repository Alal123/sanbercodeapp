import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, StatusBar} from 'react-native';
import {colors} from '../../configs';
import axios from 'axios';
import api from '../../api';
import {GiftedChat} from 'react-native-gifted-chat';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Inbox = () => {
  const [user, setUser] = useState({});
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        if (token !== null) {
          return getProfile(token);
        }
      } catch (error) {
        console.log(error);
      }
    };
    getToken();
    onRef();

    return () => {
      const db = database().ref('messages');
      if (db) {
        db.off();
      }
    };
  }, []);

  const getProfile = (token) => {
    axios
      .get(`${api}/profile/get-profile`, {
        headers: {
          Authorization: 'Bearer' + token,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
      .then((res) => {
        console.log('Account -> res', res);
        const data = res.data.data.profile;
        setUser(data);
      })
      .catch((err) => {
        console.log('Account -> err', err);
      });
  };

  const header = () => {
    return (
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Inbox</Text>
      </View>
    );
  };

  const onRef = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        const value = snapshot.val();
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, value),
        );
      });
  };

  const onSend = (message = []) => {
    for (let i = 0; i < message.length; i++) {
      database().ref('messages').push({
        _id: message[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: message[i].text,
        user: message[i].user,
      });
    }
  };
  const photopath = `https://crowdfunding.sanberdev.com/${user.photo}`;
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
      {header()}
      <GiftedChat
        messages={messages}
        onSend={(message) => onSend(message)}
        user={{
          _id: user.id,
          name: user.name,
          avatar: photopath,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  headerContainer: {
    height: 65,
    backgroundColor: 'rgba(52,137,247,1)',
    justifyContent: 'center',
    paddingLeft: 16,
  },
  headerText: {
    fontWeight: 'bold',
    color: '#ffff',
    fontSize: 20,
  },
});

export default Inbox;
