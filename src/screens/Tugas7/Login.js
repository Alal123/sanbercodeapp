import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TextInput, Keyboard} from 'react-native';
import {Header, Button} from '../../components/index';
import {colors} from '../../configs/index';
import axios from 'axios';
import api from '../../api';
import Asyncstorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import Icons from 'react-native-vector-icons/Ionicons';
import TouchID from 'react-native-touch-id';
import {CommonActions} from '@react-navigation/native';

const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

const Login = (props) => {
  const {navigation} = props;
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const saveToken = async (token) => {
    try {
      await Asyncstorage.setItem('token', token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '1091378500174-9ffs9jo6slrj7lno4pp7iia4aace0o30.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log('signInWithGoogle -> idToken', idToken);
      if (idToken) {
        const credential = auth.GoogleAuthProvider.credential(idToken);
        auth().signInWithCredential(credential);
        navigation.navigate('Profile');
      }
    } catch (error) {
      console.log('sigInWidthGoogle -> error', error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        alert('login google tidak berhasil');
        // play services not available or outdated
      } else {
        // some other error happened
        alert('login google tidak berhasil');
      }
    }
  };

  const signInWithFingerprint = () => {
    TouchID.authenticate('', config)
      .then((success) => {
        navigation.navigate('Profile');
      })
      .catch((error) => {
        console.log('error', error);
        alert('Authentication Failed');
      });
  };

  const onLogin = async () => {
    if (userName === '' || password === '') {
      alert('harap isi username atau password');
      return;
    }
    try {
      const data = {
        email: userName,
        password,
      };
      const response = await axios.post(`${api}/auth/login`, data, {
        timeout: 2000,
      });
      saveToken(response.data.data.token);
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: 'MainTab'}],
        }),
      );
      Keyboard.dismiss();
    } catch (error) {
      alert('username atau password salah');
      console.log('error', error);
    }
  };

  const renderContent = () => {
    return (
      <View style={styles.contentContainer}>
        <View style={styles.content}>
          <Text style={styles.titleContent}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor,
            vitae repudiandae ipsam a excepturi odio libero et iusto obcaecati
            exercitationem est porro veritatis quisquam sit ad ullam sunt
            cupiditate minus.
          </Text>
          <TextInput
            underlineColorAndroid={colors.lightGrey}
            placeholder="Nomor Ponsel atau Email"
            onChangeText={(text) => setUserName(text)}
          />
          <TextInput
            underlineColorAndroid={colors.lightGrey}
            placeholder="Password"
            onChangeText={(text) => setPassword(text)}
            secureTextEntry
          />
          <Button style={styles.btnLogin} onPress={onLogin}>
            <Text style={styles.loginText}>Login</Text>
          </Button>
          <Text style={styles.info}>Belum Punya Akun? Daftar</Text>
        </View>
        <View style={styles.btnBottomContainer}>
          <Text style={styles.bottmText}>Atau lebih depat dengan</Text>
          <View style={styles.btnInnerContainer}>
            <Button
              style={styles.btnFingerContainer}
              onPress={() => signInWithFingerprint()}>
              <Icons name="finger-print-outline" size={20} />
              <Text>FingerPrint</Text>
            </Button>
            <GoogleSigninButton
              onPress={() => signInWithGoogle()}
              style={styles.buttonGoogleContainer}
              size={GoogleSigninButton.Size.Wide}
              color={GoogleSigninButton.Color.Dark}
            />
          </View>
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header title="Masuk" onPress={() => navigation.pop()} />
      {renderContent()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
  titleContent: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  content: {
    paddingHorizontal: 16,
    paddingVertical: 16,
    height: 280,
    justifyContent: 'space-between',
  },
  loginText: {
    fontWeight: 'bold',
    color: colors.white,
    fontSize: 18,
  },
  btnLogin: {
    backgroundColor: colors.blue,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
  },
  info: {
    textAlign: 'center',
    marginTop: 10,
  },
  bottmText: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 18,
  },
  btnBottomContainer: {
    justifyContent: 'space-between',
    height: 90,
    marginBottom: 16,
  },
  btnInnerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnFingerContainer: {
    elevation: 2,
    backgroundColor: colors.white,
    width: '40%',
    height: 30,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
  },
  buttonGoogleContainer: {
    width: '50%',
    height: 40,
  },
});

export default Login;
