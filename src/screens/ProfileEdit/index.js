import React, {useEffect, useRef, useState} from 'react';
import {View, Text, TextInput, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {colors} from '../../configs/index';
import {Header} from '../../components/index';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icons from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

const ProfileEdit = ({navigation, route}) => {
  let input = useRef(null);
  let camera = useRef(null);
  const [editable, setEditable] = useState(false);
  const [token, setToken] = useState('');
  const [name, setName] = useState(route.params.name);
  const [email, setEmail] = useState(route.params.email);
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);
  const defaultPhoto = route.params.photo;

  useEffect(() => {
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        if (token !== null) {
          setToken(token);
        }
      } catch (error) {
        console.log('error', error);
      }
    };
    getToken();
  }, []);

  const editData = () => {
    setEditable(!editable);
  };

  const renderPhoto = () => {
    return (
      <View style={styles.photoOutsideContainer}>
        <Image source={{uri: defaultPhoto}} style={styles.photoContainer} />
        <View style={styles.fixedIconContainer}>
          <View style={styles.iconContainer}>
            <Icons name="camera" size={15} color={colors.white} />
          </View>
        </View>
      </View>
    );
  };

  const renderForm = () => {
    return (
      <View style={styles.formContainer}>
        <View>
          <Text>Nama Lengkap</Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TextInput
              value={name}
              editable={editable}
              onChangeText={(text) => setName(text)}
            />
            <TouchableOpacity onPress={() => editData()}>
            <Icons name="edit-2" size={15} color={colors.grey} />
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <Text>Email</Text>
          <TextInput value={email} editable={false} />
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header onPress={() => navigation.pop()} title="Profile Saya" />
      {renderPhoto()}
      {renderForm()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  photoOutsideContainer: {
    alignItems: 'center',
    marginTop: 16,
  },
  photoContainer: {
    width: 80,
    height: 80,
    borderRadius: 40,
  },
  fixedIconContainer: {
    position: 'absolute',
    bottom: 0,
    width: 80,
    height: 30,
    alignItems: 'flex-end',
  },
  iconContainer: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: colors.grey,
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    paddingHorizontal: 16,
    marginTop: 16,
  },
});

export default ProfileEdit;
