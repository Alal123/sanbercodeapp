import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {colors} from '../../configs';
import {Header, Chart} from '../../components';

const Statistik = (props) => {
  const {navigation} = props;
  return (
    <View style={styles.container}>
      <Header title="Statistik" onPress={() => navigation.pop()} />
      <Chart />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});

export default Statistik;
