import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Header, Button} from '../../components';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {colors} from '../../configs';
import Icons from 'react-native-vector-icons/Ionicons';
import api from '../../api';
import axios from 'axios';

const OtpRegister = (props) => {
  const [otpCode, setOtp] = useState('');
  const {navigation, route} = props;
  const {
    params: {email},
  } = route;

  const onVerify = async () => {
    if (otpCode === '') {
      return;
    }
    try {
      const response = await axios.post(`${api}/auth/verification`, {
        otp: otpCode,
      });
      if (response.data.response_message === 'OTP Code tidak ditemukan') {
        alert('OTP Code tidak ditemukan');
        return;
      }
      navigation.navigate('SetupPassword', {
        email,
      });
    } catch (error) {
      console.log('error', error);
    }
  };

  const onResend = async () => {
    try {
      await axios.post(
        `${api}/auth/regenerate-otp`,
        {email},
        {
          timeout: 2000,
        },
      );
    } catch (error) {
      console.log('error', error);
    }
  };

  const renderBottom = () => {
    return (
      <View>
        <Button style={styles.btVerify} onPress={onVerify}>
          <Text style={styles.verifyText}>VERIFIKASI</Text>
        </Button>
        <View style={styles.verifybottom}>
          <Text style={{color: colors.grey}}>
            Belum Menerima Kode Verifikasi
          </Text>
          <Button onPress={onResend}>
            <Text style={{color: colors.blue}}>Kirim Ulang</Text>
          </Button>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header title="Daftar" onPress={() => navigation.pop()} />
      <View style={styles.contentContainer}>
        <Text style={styles.title}>Perjalanan Kebaikanmu dimulai di sini</Text>
        <Text style={styles.description}>
          Masukkan 6 digit kode yang kami kirim ke{' '}
          <Text style={{fontWeight: 'bold'}}>{email}</Text>
        </Text>
        <OTPInputView
          autoFocusOnLoad
          pinCount={6}
          code={otpCode}
          onCodeChanged={(code) => setOtp(code)}
          style={{height: 150}}
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
        />
        {renderBottom()}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  contentContainer: {
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  description: {
    marginTop: 10,
  },
  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    color: colors.black,
  },
  underlineStyleHighLighted: {
    borderColor: colors.grey,
  },
  btVerify: {
    backgroundColor: colors.blue,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
  },
  verifyText: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: 16,
  },
  verifybottom: {
    marginTop: 16,
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 50,
  },
});

export default OtpRegister;
