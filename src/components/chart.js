import React, {useState} from 'react';
import {View, Text, processColor} from 'react-native';
import {colors} from '../configs';
import {BarChart} from 'react-native-charts-wrapper';
const chart = () => {
  const [legend, setLegend] = useState({
    enabled: false,
    textSize: 14,
    form: 'SQUARE',
    fontSize: 14,
    xEntrySpace: 10,
    yEnterySpace: 5,
    formToTextSpace: 5,
    wordWrapEnabled: true,
    maxSizePercent: 0.5,
  });
  const [chart, setChart] = useState({
    data: {
      dataSets: [
        {
          values: [
            {y: 100},
            {y: 60},
            {y: 90},
            {y: 45},
            {y: 67},
            {y: 32},
            {y: 150},
            {y: 70},
            {y: 40},
            {y: 89},
          ],
          label: '',
          config: {
            color: [processColor(colors.blue)],
            stacklabels: [],
            drawFilled: false,
            drawValues: false,
          },
        },
      ],
    },
  });

  const [xAxis, setXaxis] = useState({
    valueFormatter: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Agu',
      'Sep',
      'Oct',
      'Nov',
      'Des',
    ],
    position: 'BOTTOM',
    drawAxisLine: true,
    drawGridLines: false,
    axisMinimum: -0.5,
    granularityEnabled: true,
    granularity: 1,
    axisMaximum: new Date().getMonth() + 0.5,
    spaceBetweenLabels: 0,
    labelRotationAngel: -45.0,
    limitLines: [{limit: 115, lineColor: processColor('red'), lineWidth: 1}],
  });

  const [yAxis, setYAxis] = useState({
    left: {
      axisMinimum: 0,
      labelcountForce: true,
      granularity: 5,
      granularityEnabled: true,
      darwGridLines: false,
    },
    right: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      enabled: false,
    },
  });

  return (
    <View style={{flex: 1}}>
      <BarChart
        style={{flex: 1}}
        data={chart.data}
        yAxis={yAxis}
        xAxis={xAxis}
        doubleTapToZoomEnabled={false}
        chartDescription={{text: ''}}
        legend={legend}
        marker={{
          enabled: true,
          markerColor: 'grey',
          textColor: 'white',
          textSize: 14,
        }}
      />
    </View>
  );
};

export default chart;
