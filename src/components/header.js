import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {colors} from '../configs/index';
import Icons from 'react-native-vector-icons/FontAwesome5';

const Header = (props) => {
  const {title, onPress} = props;
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress}>
        <Icons name="arrow-left" size={20} color={colors.white} />
      </TouchableOpacity>

      <Text style={styles.titleText}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 56,
    backgroundColor: colors.blue,
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  titleText: {
    fontSize: 15,
    color: colors.white,
    marginLeft: 20,
    fontWeight: 'bold',
  },
});

export default Header;
