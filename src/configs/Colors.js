const colors = {
  white: '#ffffff',
  blue: '#3a86ff',
  darkblue: '#088dc4',
  black: '#000000',
  grey: '#A0A0A0',
  lightGrey: '#e6e6e6',
  transparent: 'rgba(0, 0, 0, 0.5)',
  red: '#FF0000',
  yellow: '#FFFF00',
};

export default colors;
